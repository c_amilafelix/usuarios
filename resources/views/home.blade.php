@extends('layouts.app')

@section('content')
<div>
    <a class="logout" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
        {{ __('Sair') }}
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</div>
<div>
    {{ $usuario->nome }}
    <form action="editar">
        <input type="text" placeholder="nome" name="nome" required value="{{ $usuario->nome }}"><br>
        <input type="email" placeholder="email" name="email" required value="{{ $usuario->email }}"><br>
        <input type="phone" placeholder="telefone" name="telefone" required value="{{ $usuario->telefone }}"><br>
        <input type="date" placeholder="nascimento" name="nascimento" required value="{{ $usuario->nascimento }}"><br>
        <input type="password" placeholder="senha" name="password" required><br>
        <button type="submit">Editar</button>

    </form>
</div>
@endsection
