<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    //

    public function welcome() 
    {
        return view('welcome');
    }


    public function cadastro(Request $request)
    {
        //dd($request->all());
        $usuario = new User;
        $usuario->nome= $request->nome;
        $usuario->email= $request->email;
        $usuario->telefone= $request->telefone;
        $usuario->nascimento= $request->nascimento;
        $usuario->password= bcrypt($request->password);
        $usuario->save();
        return "salvo";  
    }

    public function home() 
    {
        $usuario= Auth::user();
        return view('home', ['usuario'=>$usuario]);
    }

    public function editar(Request $request)
    {
        $usuario = Auth::user();
        $usuario->nome= $request->nome;
        $usuario->email= $request->email;
        $usuario->telefone= $request->telefone;
        $usuario->nascimento= $request->nascimento;
        $usuario->password= bcrypt($request->password);
        $usuario->save();
        return "salvo";  
    }
}
